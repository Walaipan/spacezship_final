﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManeger : MonoBehaviour
{
    public Text gameOverText;
    public Text restartText;

    private bool gameOver;
    private bool restart;

    void Start()
    {
        gameOver = false;
        restart = false;

        restartText.text = "";
        gameOverText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if(gameOver)
        {
            restart = true;
            restartText.text = "Prees [R] for Restart!";
        }

        if(restart)
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }

    public void BeginGame()
    {
        //gamePlaying = True;
    }
}
