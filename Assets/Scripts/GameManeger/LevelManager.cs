﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public GameObject PauseButton;

    public GameObject PauseUI;
    public GameObject GameEndUI;

    public TextMeshProUGUI ScoreText;
    public Text PresentScoreText;
    public GameManager GameManager;

    void Awake()
    {
        PauseButton.SetActive(true);
        PauseUI.SetActive(false);
        GameEndUI.SetActive(false);
        Time.timeScale = 1;
    }

    public void Update()
    {
        PresentScoreText.text = $"Score: {GameManager.Score.ToString("0")}";
    }

    public void PauseGame()
    {
        PauseButton.SetActive(false);
        PauseUI.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        PauseButton.SetActive(true);
        PauseUI.SetActive(false);
        Time.timeScale = 1;
    }

    public void GameEnd()
    {
        PauseButton.SetActive(false);
        ScoreText.text = $"Score: {GameManager.Score.ToString("0")}";
        GameEndUI.SetActive(true);
        Time.timeScale = 0;

    }
}
